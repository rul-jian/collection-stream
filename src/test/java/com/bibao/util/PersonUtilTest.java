package com.bibao.util;

import static org.junit.Assert.*;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.bibao.model.Gender;
import com.bibao.model.Person;
import com.bibao.model.SimplePerson;

public class PersonUtilTest {
	private static final Logger LOG = Logger.getLogger(PersonUtilTest.class);
	private List<Person> personList;
	
	@Before
	public void setUp() throws Exception {
		personList = PersonUtil.createPersonList();
	}

	@Test
	public void testFindById() {
		Person person = PersonUtil.findById(1, personList);
		assertNotNull(person);
		assertEquals("Alice", person.getFirstName());
		assertEquals("Zhang", person.getLastName());
		person = PersonUtil.findById(100, personList);
		assertNull(person);
	}
	
	@Test
	public void testFindSortedNames() {
		List<String> sortedNames = PersonUtil.findSortedNames(personList);
		assertEquals(7, sortedNames.size());
		sortedNames.forEach(name -> LOG.debug(name));
	}

	@Test
	public void testConvertToSortedSimplePersonList() {
		List<SimplePerson> list = PersonUtil.convertToSortedSimplePersonList(personList);
		assertEquals(7, list.size());
		list.forEach(person -> LOG.debug(person));
	}
	
	@Test
	public void testFindByFirstNameAndLastName() {
		Optional<Person> person = PersonUtil.findByFirstNameAndLastName(personList, "James", "Lin");
		assertTrue(person.isPresent());
		LOG.debug(person.get());
		person = PersonUtil.findByFirstNameAndLastName(personList, "James", "Lee");
		assertFalse(person.isPresent());
	}
	
	@Test
	public void testGroupByGender() {
		Map<Gender, List<Person>> genderMap = PersonUtil.groupByGender(personList);
		assertEquals(2, genderMap.size());
		assertEquals(4, genderMap.get(Gender.MALE).size());
		assertEquals(3, genderMap.get(Gender.FEMALE).size());
		genderMap.forEach((gender, value) -> {
			LOG.debug(gender);
			value.forEach(p -> LOG.debug(p));
		});
	}
	
	@Test
	public void testGetMaxSalary_Success() {
		assertEquals(92600, PersonUtil.getMaxSalary(personList).intValue());
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testGetMaxSalary_Fail() {
		PersonUtil.getMaxSalary(new ArrayList<>());
	}
	
	@Test
	public void testGetYoungestPerson_Success() {
		Person person = PersonUtil.findYoungestPerson(personList);
		assertEquals(28, person.getAge());
		LOG.debug(person);
	}
	
	@Test(expected=NoSuchElementException.class)
	public void testGetYoungestPerson_Fail() {
		PersonUtil.findYoungestPerson(new ArrayList<>());
	}
	
	@Test
	public void testFindAvgSalary() {
		double avgSal = PersonUtil.findAvgSalary(personList);
		DecimalFormat df = new DecimalFormat("$###,###.00");
		LOG.debug(df.format(avgSal));
	}
}
