package com.bibao.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import com.bibao.model.Gender;
import com.bibao.model.Person;
import com.bibao.model.SimplePerson;

public class PersonUtil {
	public static List<Person> createPersonList() {
		List<Person> list = new ArrayList<>();
		list.add(createPerson(1, "Alice", "Zhang", Gender.FEMALE, 35, 43500));
		list.add(createPerson(2, "Bob", "Liang", Gender.MALE, 42, 32600));
		list.add(createPerson(3, "Steven", "Wang", Gender.MALE, 55, 78200));
		list.add(createPerson(4, "Alex", "Chen", Gender.MALE, 28, 56500));
		list.add(createPerson(5, "Mary", "Ding", Gender.FEMALE, 31, 92600));
		list.add(createPerson(6, "Jennifer", "Zhao", Gender.FEMALE, 51, 65400));
		list.add(createPerson(7, "James", "Lin", Gender.MALE, 52, 76300));
		return list;
	}
	
	public static Person findById(int id, List<Person> personList) {
		Optional<Person>  person = personList.stream().filter(p -> p.getId() == id).findAny();
		return person.isPresent()? person.get() : null;
	}
	
	public static List<String> findSortedNames(List<Person> personList) {
		return personList.stream()
				.map(p -> p.getLastName() + ", " + p.getFirstName())
				.sorted()
				.collect(Collectors.toList());
	}
	
	public static List<SimplePerson> convertToSortedSimplePersonList(List<Person> personList) {
		return personList.stream()
				.sorted((p1, p2) -> p1.getSalary() - p2.getSalary())
				.map(p -> mapToSimplePerson(p))
				.collect(Collectors.toList());
	}
	
	public static Optional<Person> findByFirstNameAndLastName(List<Person> personList, String firstName, String lastName) {
		return personList.stream()
				//.filter(p -> (p.getLastName().equals(lastName) && p.getFirstName().equals(firstName)))
				.filter(p -> p.getLastName().equals(lastName))
				.filter(p -> p.getFirstName().equals(firstName))
				.findFirst();
	}
	
	public static Map<Gender, List<Person>> groupByGender(List<Person> personList) {
		return personList.stream()
				.collect(Collectors.groupingBy(Person::getGender));
	}
	
	public static Integer getMaxSalary(List<Person> personList) {
		return personList.stream()
				.mapToInt(Person::getSalary)
				.max()
				.orElseThrow(NoSuchElementException::new);
	}
	
	public static Person findYoungestPerson(List<Person> personList) {
		return personList.stream()
				.min(Comparator.comparing(Person::getAge))
				.orElseThrow(NoSuchElementException::new);
	}
	
	public static double findAvgSalary(List<Person> personList) {
		return personList.stream()
				.collect(Collectors.averagingInt(Person::getSalary));
	}
	
	/**************************************
	 * Private Methods for Internal Usage *
	 **************************************/
	
	private static Person createPerson(int id, String firstName, String lastName, Gender gender, int age, int salary) {
		Person person = new Person();
		person.setId(id);
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setGender(gender);
		person.setAge(age);
		person.setSalary(salary);
		return person;
	}
	
	private static SimplePerson mapToSimplePerson(Person person) {
		DecimalFormat df = new DecimalFormat("$###,###.00");
		SimplePerson simplePerson = new SimplePerson();
		simplePerson.setName(person.getFirstName() + " " + person.getLastName());
		simplePerson.setGender(person.getGender().name());
		simplePerson.setSalary(df.format(person.getSalary()));
		return simplePerson;
	}
}
